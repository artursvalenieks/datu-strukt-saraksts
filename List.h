#ifndef LIST_H
#define LIST_H
#include <iostream>

class IndexOutOfRangeException {};
class ElementNotFoundException {};
class LastElementException {};

template <typename T>
class List {
public:

	List(unsigned int length_to_reserve = 2) : m_occupied_length(0) {
		m_reserved_length = length_to_reserve ? length_to_reserve : 1;
		m_buffer = m_create_buffer(m_reserved_length);
		
	}

	List(const List<T> &obj) {
		m_reserved_length = obj.m_reserved_length;
		m_occupied_length = obj.m_occupied_length;
		m_buffer = m_create_buffer(m_reserved_length);
		m_copy_buffer(obj.m_buffer, 0, m_buffer, 0, m_occupied_length);
	}

	~List() {
		m_delete_buffer(m_buffer);
	}

	List<T> operator=(const List<T> &obj) {
		m_reserved_length = obj.m_reserved_length;
		m_occupied_length = obj.m_occupied_length;
		m_buffer = m_create_buffer(m_reserved_length);
		m_copy_buffer(obj.m_buffer, 0, m_buffer, 0, m_occupied_length);
		return *this;
	}

	void add(const T &element) {
		m_add_element_at_index(element, m_occupied_length);
	}

	void add(const T &element, unsigned int index) {
		if (m_is_new_element_index_valid(index)) {
			m_add_element_at_index(element, index);
		}
		else {
			throw new IndexOutOfRangeException;
		}
	}

	T get(unsigned int index)const {
		if (m_is_existing_element(index)) {
			return m_buffer[index];
		}
		else {
			throw new IndexOutOfRangeException;
		}
	}

	T getNext(const T &element)const {
		unsigned int next_element_index = m_get_element_index(element) + 1;
		if (m_is_existing_element(next_element_index)) {
			return m_buffer[next_element_index];
		}
		else {
			throw new LastElementException;
		}
	}

	unsigned int getIndex(const T &element)const {
		return m_get_element_index(element);
	}

	void drop(unsigned int index) {
		if (m_is_existing_element(index)) {
			m_drop_element(index);
		}
		else {
			throw new IndexOutOfRangeException;
		}
	}

	void empty(unsigned int reserved_length = 10) {
		m_reserved_length = (reserved_length > 0) ? reserved_length : 1;
		m_delete_buffer(m_buffer);
		m_buffer = m_create_buffer(m_reserved_length);
		m_occupied_length = 0;
	}

	unsigned int length()const {
		return m_occupied_length;
	}

	void print()const {
		for (unsigned int index = 0; index < m_occupied_length; index++) {
			std::cout << m_buffer[index] << std::endl;
		}
	}

	void sortAscending() {
		m_sort(true);
	}

	void sortDescending() {
		m_sort(false);
	}

	
private:
	T* m_buffer;
	unsigned int m_reserved_length;
	unsigned int m_occupied_length;

	//ADDING RELATED METHODS
	bool m_is_new_element_index_valid(unsigned int index)const {
		return index <= m_occupied_length;
	}

	void m_add_element_at_index(const T &element, unsigned int index) {
		if (m_is_buffer_full()) {
			m_resize_buffer(m_reserved_length * 2);
		}

		if (m_is_first_free_space(index)) {
			m_add_new_element_to_buffer(m_buffer, element, index);
			m_occupied_length++;
		}
		else {
			m_strech_occupied_buffer_part(index);
			m_add_new_element_to_buffer(m_buffer, element, index);
		}
	}

	//REMOVING RELATED METHODS
	bool m_is_existing_element(unsigned int index)const {
		return index < m_occupied_length;
	}

	void m_drop_element(unsigned int squeeze_index) {
		for (int index = squeeze_index; index < m_occupied_length - 1; index++) {
			m_buffer[index] = m_buffer[index + 1];
		}
		m_occupied_length--;
	}

	bool m_is_first_free_space(unsigned int index)const {
		return m_occupied_length == index;
	}
		
	//SORTING RELATED METHODS
	void m_sort(bool arrangment) {
		for (int index = 1; index < m_occupied_length; index++) {
			for (int index2 = 0; index2 < m_occupied_length - index; index2++) {
				if ((m_buffer[index2] > m_buffer[index2 + 1] && arrangment) ||
					(m_buffer[index2] < m_buffer[index2 + 1] && !arrangment)) {
					m_swap_elements(index2, index2 + 1);
				}
			}
		}
	}
	
	void m_swap_elements(unsigned int index, unsigned int index2) {
		T temp = m_buffer[index];
		m_buffer[index] = m_buffer[index2];
		m_buffer[index2] = temp;
	}
	
	//GET, SEARCH RELATED METHODS
	unsigned int m_get_element_index(const T &element)const {
		for (int index = 0; index < m_occupied_length; index++) {
			if (m_buffer[index] == element) {
				return index;
			}
		}
		throw new ElementNotFoundException;
	}

	//BUFFER RELATED METHODS
	T* m_create_buffer(unsigned int length) {
		return (T*)(new char[sizeof(T) * length]);
	}

	bool m_is_buffer_full()const {
		return m_occupied_length == m_reserved_length;
	}

	void m_resize_buffer(unsigned int length) {
		T* resized_buffer = m_create_buffer(length);
		m_copy_buffer(m_buffer, 0, resized_buffer, 0, m_occupied_length);
		m_delete_buffer(m_buffer);
		m_buffer = resized_buffer;
		m_reserved_length = length;
	}

	void m_copy_buffer(T* src, unsigned int src_start, T *dest, unsigned int dest_start, unsigned int elements_to_copy) {
		for (unsigned int index = 0; index < elements_to_copy; index++) {
			m_add_new_element_to_buffer(dest, src[src_start + index], dest_start + index);
		}
	}

	void m_delete_buffer(T* buffer) {
		delete[](char*)buffer;
	}

	void m_add_new_element_to_buffer(T* buffer, T element, unsigned int index) {
		new(buffer + index) T(element);
	}

	void m_strech_occupied_buffer_part(unsigned int strech_index) {
		m_add_new_element_to_buffer(m_buffer, m_buffer[m_occupied_length - 1], m_occupied_length);
		for (int index = m_occupied_length - 1; index >= strech_index && index; index--) {
			m_buffer[index] = m_buffer[index - 1];
		}
		m_occupied_length++;
	}

};

#endif //LIST_H

