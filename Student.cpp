#include "Student.h"

unsigned int Student::m_student_idCounter = 0;

Student::Student() {
	m_student_id = m_student_idCounter;
	m_student_idCounter++;
	m_age = 0;
	m_gender = "none";
	m_first_name = "NONE";
	m_last_name = "NONE";
}

Student::~Student() {
	m_student_idCounter--;
}

unsigned int Student::getStudentID()const {
	return m_student_id;
}

std::string Student::getGender()const {
	return m_gender;
}

std::string Student::getFirstName()const {
	return m_first_name;
}
std::string Student::getLastName()const {
	return m_last_name;
}
unsigned short int Student::getAge()const {
	return m_age;
}

void Student::setFirstName(std::string first_name) {
	m_first_name = first_name;
	m_set_gender(m_first_name);
}

void Student::setLastName(std::string last_name) {
	m_last_name = last_name;
}

void Student::setAge(unsigned short int age) {
	m_age = age;
}

Student Student::operator=(const Student &obj) {
	m_student_idCounter++;
	m_first_name = obj.m_first_name;
	m_last_name = obj.m_last_name;
	m_student_id = obj.m_student_id;
	m_age = obj.m_age;
	m_gender = obj.m_gender;
	m_grades = obj.m_grades;
	return *this;
}

bool operator==(const Student &lhs, const Student &rhs) {
	return lhs.getStudentID() == rhs.getStudentID();
}

std::ostream& operator<<(std::ostream& os, Student &obj) {
	os << obj.getInfo();
	return  os;
}

std::string Student::getInfo()const {
	std::stringstream ss;
	ss << "ID " << getStudentID() << ", name: " << getFirstName() << ", last-name: " << getLastName() << ", gender: " << getGender() <<", age: " << getAge() << ", grades: " << getGrades() << ", avg: " << getAvarageGrade();
	return ss.str();
}

std::string Student::getGrades()const {
	std::stringstream ss;
	for (int index = 0; index < m_grades.length(); index++) {
		ss << m_grades.get(index) << " ";
	}
	return ss.str();
}

double Student::getAvarageGrade()const {
	double sum = 0;
	for (int index = 0; index < m_grades.length(); index++) {
		sum += m_grades.get(index);
	}
	return m_grades.length() ? sum / m_grades.length() : 0;
}

void Student::addGrade(unsigned short int grade) {
	if (m_is_grade_valid(grade)) {
		m_grades.add(grade);
	}
	else {
		throw new BadGradeValueException;
	}
}

bool Student::m_is_grade_valid(unsigned short int grade)const {
	return grade >= 0 && grade <= 10;
}

void Student::removeGrade(unsigned int index) {
	m_grades.drop(index);
}

void Student::m_set_gender(std::string first_name) {
	char last_char = first_name.at(first_name.size()-1);
	if (last_char == 's' || last_char == 'o') { // otto, arturs
		m_gender = "male";
	}
	else {
		m_gender = "female";
	}
}

bool operator<(const Student& lhs, const Student& rhs) {
	return lhs.getStudentID() < rhs.getStudentID();
}

bool operator>(const Student& lhs, const Student& rhs) {
	return lhs.getStudentID() > rhs.getStudentID();
}
