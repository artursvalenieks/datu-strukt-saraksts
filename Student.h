#pragma once

#include "List.h"
#include <string>
#include <sstream>
#include <iostream>

class BadGradeValueException {};

class Student
{
public:
	Student();
	~Student();
	
	unsigned int getStudentID()const;
	std::string getGender()const;
	std::string getFirstName()const;
	std::string getLastName()const;
	unsigned short int getAge()const;
	std::string getInfo()const;
	std::string getGrades()const;
	double getAvarageGrade()const;

	void setFirstName(std::string first_name);
	void setLastName(std::string last_name);
	void setAge(unsigned short int age);

	void addGrade(unsigned short int grade);
	void removeGrade(unsigned int index);

	Student operator=(const Student &obj);
	

private:
	static unsigned int m_student_idCounter;

	List<int> m_grades;
	std::string m_first_name;
	std::string m_last_name;
	unsigned int m_student_id;
	unsigned short int m_age;
	std::string m_gender;

	void m_set_gender(std::string first_name);
	bool m_is_grade_valid(unsigned short int grade)const;
};

bool operator==(const Student &lhs, const Student &rhs);
bool operator<(const Student &lhs, const Student &rhs);
bool operator>(const Student &lhs, const Student &rhs);
std::ostream& operator<<(std::ostream& os, Student &obj);