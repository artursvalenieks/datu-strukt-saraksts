#include "List.h"
#include "Student.h"

#include <iostream>
#include <string>
#include <fstream>

using namespace std;

int main() {

	std::string line;
	std::ifstream moodleFiles;
	List<int> saraksts = List<int>(10);
    int temp;
	moodleFiles.open("skaitli.txt");
	if (moodleFiles.is_open()) {
		while (!moodleFiles.eof()) {
            moodleFiles >> temp;
			saraksts.add(temp);
		}
	}
	else{
		std::cout << "Failu neizdevas atvert!";
	}
	saraksts.print();
	std::cout << "Saraksta garums:" << saraksts.length() << std::endl;
	std::cout << "Pievienojam PIECI 2 indeksaa:" << std::endl;
	saraksts.add(5, 2);
	saraksts.print();
	std::cout << "Pievienojam PIECI 99 indeksaa:" << std::endl;
	try{
		saraksts.add(5, 99);
	}
	catch (IndexOutOfRangeException *e) {
		std::cout << "nezizdevaas pievienot.\n";
	}
	std::cout << "Dzesham lauka pieci.\n";
	saraksts.drop(2);
	saraksts.print();
	std::cout << "Izgustam pedejo elementu:" << saraksts.get(saraksts.length() - 1) << std::endl;
	std::cout << "meginam izguut 234 elementu.\n";
	try{
		saraksts.get(234);
	}
	catch (IndexOutOfRangeException *e) {
		std::cout << "Tads indeks neeksiste\n";
	}
	std::cout << "Spiediet enter lai turpinatu:\n";
	std::cin.ignore();
	saraksts.add(11, 2);
	saraksts.print();
	std::cout << "Izgustam naakamo elementu peec 11:" << saraksts.getNext(11) << std::endl;
	std::cout << "meginam izguut naakamo peec peedeejaa elementu.\n";
	try{
		saraksts.getNext(saraksts.get(saraksts.length() - 1));
	}
	catch (LastElementException *e) {
		std::cout << "Neizdevas izguut naakamo peec peedeejaa elementu.\n";
	}

	std::cout << "meginam izguut neesosu elementu.\n";
	try{
		saraksts.getNext(5461);
	}
	catch (ElementNotFoundException *e) {
		std::cout << "Nav tads elements\n";
	}

	std::cout << "Izgustam naakamo element1 11 indeksu:" << saraksts.getIndex(11) << std::endl;

	std::cout << "meginam izguut neesosa elementa indeksu\n";
	try{
		saraksts.getIndex(24235425);
	}
	catch (ElementNotFoundException *e) {
		std::cout << "Nezidevas atrast elementu\n";
	}

	std::cout << "Kartojam augosa seciba:\n";
	saraksts.sortAscending();
	saraksts.print();
	std::cout << "Kartojam dilstosha seciba:\n";
	saraksts.sortDescending();
	saraksts.print();

	std::cout << "Iztuksojam sarakstu:\n";
	saraksts.empty();
	saraksts.print();

	std::cout << "Spiediet enter lai turpinatu:\n";
	std::cin.ignore();

	List<Student> studenti;

	Student arturs = Student();

	arturs.setFirstName("Arturs");
	arturs.setLastName("Valenieks");
	arturs.setAge(21);
	arturs.addGrade(10);
	arturs.addGrade(9);
	arturs.addGrade(7);

	Student edgars;
	edgars.setFirstName("Edgars");
	edgars.setLastName("Pacans");
	edgars.setAge(16);
	edgars.addGrade(2);
	edgars.addGrade(3);
	edgars.addGrade(7);

	Student inese;
	inese.setFirstName("Inese");
	inese.setLastName("Pacane");
	inese.setAge(14);
	inese.addGrade(7);
	inese.addGrade(3);
	inese.addGrade(7);
	inese.addGrade(3);
	inese.addGrade(3);
	inese.addGrade(7);
	inese.addGrade(7);

	Student ieva;
	ieva.setFirstName("Ieva");
	ieva.setLastName("Kurme");
	ieva.setAge(44);
	ieva.addGrade(2);
	ieva.addGrade(8);
	ieva.addGrade(9);

	studenti.add(arturs);
	studenti.add(edgars);
	studenti.add(ieva);
	studenti.add(inese);



	studenti.print();

	studenti.sortAscending();
	studenti.sortAscending();
	studenti.get(2);
	studenti.getNext(inese);
	std::cin.ignore();
	std::cout << "Kartojam augosa seciba\n";
	studenti.sortAscending();
	studenti.print();
	std::cout << "Kartojam dilstosa seciba\n";
	studenti.sortDescending();
	studenti.print();

	std::cout << "Atgriezam nakoso pec ievas\n";
	std::cout << studenti.getNext(ieva).getInfo();

	std::cout << "\nIevas index ir" << studenti.getIndex(ieva) <<"\n";

	cout << "Tiekam valja Edgara\n";
	studenti.drop(studenti.getIndex(edgars));
	studenti.print();
	cout << "Tiekam vala no parejiem Studentiem\n";
	studenti.empty();
	studenti.print();
	cout << "beigas\n";

std::cin.ignore();
return 0;
}
